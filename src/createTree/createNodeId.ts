import { TreeContext, NodeId } from "./types";

const counters = new Map<TreeContext, number>();

export function createNodeId(context: TreeContext): NodeId {
  const counter = counters.get(context) || 0;
  const next = counter + 1;
  counters.set(context, next);
  return { nodeId: (next) };
}
