import { flatMap } from "@b08/array";
import { KeySet } from "@b08/flat-key";
import { InterfaceModel, TypeId } from "@b08/type-parser";
import { getStateByDefinition, stateModels } from "./stateModels";
import { TreeContext } from "./types";

export function getParentlessStates(context: TreeContext): TypeId[] {
  const children = flatMap(stateModels(context), m => getChildStates(m, context));
  const childrenSet = new KeySet(children);
  return stateModels(context)
    .map(state => state.id)
    .filter(id => !childrenSet.has(id));
}

function getChildStates(sm: InterfaceModel, context: TreeContext): TypeId[] {
  return sm.fields.map(f => getStateByDefinition(f.fieldType, context))
    .filter(state => state != null)
    .map(state => state.id);
}
