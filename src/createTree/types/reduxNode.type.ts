import { TypeId } from "@b08/type-parser";

export interface ReduxNode {
  id: NodeId;
  stateId: TypeId;
  fields: NodeField[];
}

export interface NodeField {
  fieldName: string;
  nodeId: NodeId;
}

export interface NodeId {
  nodeId: number;
}
