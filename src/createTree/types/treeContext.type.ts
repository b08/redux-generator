import { ParsedModel } from "../../types";

export interface TreeContext {
  parsed: ParsedModel[];
}
