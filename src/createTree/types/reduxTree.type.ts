import { ReduxNode, NodeId } from "./reduxNode.type";

export interface ReduxTree {
  nodes: ReduxNode[];
  roots: NodeId[];
}
