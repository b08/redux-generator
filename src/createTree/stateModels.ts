import { InterfaceModel, TypeDefinition, TypeId } from "@b08/type-parser";
import { getInterfaceById, interfaces, isImportableMonotype } from "@b08/type-parser-methods";
import { reduxConstants } from "../redux.const";
import { TreeContext } from "./types";

export function stateModels(context: TreeContext): InterfaceModel[] {
  return interfaces(context.parsed, isState);
}

function isState(model: InterfaceModel): boolean {
  return model.extends.length === 0 &&
    model.methods.length === 0 &&
    !model.fields.some(f => f.isOptional) &&
    isStateName(model.id.name);
}

const isStateName = (name: string) => name.endsWith(reduxConstants.stateSuffix);

export function getStateById(id: TypeId, context: TreeContext): InterfaceModel {
  const state = getInterfaceById(id, context.parsed);
  return state && isState(state) ? state : null;
}

export function getStateByDefinition(def: TypeDefinition, context: TreeContext): InterfaceModel {
  return isImportableMonotype(def) ? getStateById(def.importedTypes[0].id, context) : null;
}
