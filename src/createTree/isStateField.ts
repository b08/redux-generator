import { FieldModel } from "@b08/type-parser";
import { getStateByDefinition } from "./stateModels";
import { TreeContext } from "./types";
import { filterWith, rejectWith } from "@b08/functional";

export const getStateFields = filterWith(isStateField);
export const getNonStateFields = rejectWith(isStateField);

function isStateField(field: FieldModel, context: TreeContext): boolean {
  return getStateByDefinition(field.fieldType, context) != null;
}
