import { memoize } from "@b08/memoize";
import { mapBy } from "@b08/flat-key";
import { NodeId, ReduxNode, ReduxTree } from "./types";

const nodesMap = memoize((tree: ReduxTree) => mapBy(tree.nodes, node => node.id));

export function getNode(nodeId: NodeId, tree: ReduxTree): ReduxNode {
  return nodesMap(tree).get(nodeId);
}
