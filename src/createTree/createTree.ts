import { flatMap } from "@b08/array";
import { mapWith } from "@b08/functional";
import { FieldModel, TypeId } from "@b08/type-parser";
import { createNodeId } from "./createNodeId";
import { getParentlessStates } from "./getParentlessStates";
import { getStateFields } from "./isStateField";
import { getStateById } from "./stateModels";
import { NodeField, NodeId, ReduxNode, ReduxTree, TreeContext } from "./types";

export function createTree(context: TreeContext): ReduxTree {
  const roots = getParentlessStates(context);
  const rootsWithNodeIds = roots.map(root => ({ root, nodeId: createNodeId(context) }));
  const nodes = flatMap(rootsWithNodeIds, root => createNodeWithChildren(root.root, root.nodeId, context));
  return {
    nodes,
    roots: rootsWithNodeIds.map(r => r.nodeId)
  };
}

function createNodeWithChildren(stateId: TypeId, id: NodeId, context: TreeContext): ReduxNode[] {
  const state = getStateById(stateId, context);
  const stateFields = getStateFields(state.fields, context);
  const fields = createNodeFields(stateFields, context);
  const node = { stateId: state.id, id: id, fields };
  const childNodes = flatMap(stateFields,
    (field, index) => createNodeWithChildren(field.fieldType.importedTypes[0].id, fields[index].nodeId, context));
  return [node, ...childNodes];
}

const createNodeFields = mapWith(function (field: FieldModel, context: TreeContext): NodeField {
  return { fieldName: field.fieldName, nodeId: createNodeId(context) };
});

