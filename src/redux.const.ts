export const reduxConstants = {
  stateSuffix: "State",
  stateIdField: "stateId",
  stateIdType: "number",
  actionsFile: "actions",
  reducerFile: "reducer",
  storeTypeFile: "store.type",
  reducerSuffix: "Reducer",
  dispatcherFile: "dispatcher",
  dispatcherSuffix: "Dispatcher",
  selectorSuffix: "Selector",
  initializerPrefix: "initialize",
  tsExtension: ".ts"
};
