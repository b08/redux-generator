import { IMap } from "@b08/object-map";

export type Quotes = "'" | "\"";
export type LineFeed = "\n" | "\r\n" | "\r";

export interface GeneratorOptions {
  linefeed?: LineFeed;
  quotes?: Quotes;
  aliasMap?: IMap;
}
