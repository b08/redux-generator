import { TypeId, FunctionModel } from "@b08/type-parser";
import { ProductionContext } from "../types/productionContext.type";
import { memoize } from "@b08/memoize";
import { KeyGroup, groupBy } from "@b08/flat-key";
import { getLoneReductions } from "../actions/getLoneActions";

export function getLoneReductionsByState(state: TypeId, context: ProductionContext): FunctionModel[] {
  return getLoneReductionsMap(context).get(state) || [];
}

const getLoneReductionsMap = memoize(
  function getLoneReductionsMap(context: ProductionContext): KeyGroup<TypeId, FunctionModel> {
    return groupBy(getLoneReductions(context), r => r.returnType.importedTypes[0].id);
  }
);
