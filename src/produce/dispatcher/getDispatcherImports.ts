import { flatMap } from "@b08/array";
import { ImportModel } from "@b08/imports-generator";
import { TypeId } from "@b08/type-parser";
import { ProductionContext } from "../types/productionContext.type";
import { dispatcherType } from "./dispatcherType";
import { getLoneReductionsByState } from "./getLoneReductionsByState";
import { getReductionClassesByState } from "./getReductionClassesByState";
import { isReductionMethod } from "../actions/getClassActions";
import { importTypeDefinitions } from "@b08/type-parser-methods";

export function getLoneDispatcherImports(states: TypeId[], context: ProductionContext): ImportModel[] {
  const dt = dispatcherType(states[0]);
  const reductions = flatMap(states, state => getLoneReductionsByState(state, context));
  return importTypeDefinitions(flatMap(reductions, r => r.parameters.slice(1)).map(p => p.parameterType), dt);
}

export function getClassDispatcherImports(states: TypeId[], context: ProductionContext): ImportModel[] {
  const dt = dispatcherType(states[0]);
  const classes = flatMap(states, state => getReductionClassesByState(state, context));
  const methods = flatMap(classes, cls => cls.methods.filter(m => isReductionMethod(m, cls)));
  return importTypeDefinitions(flatMap(methods, m => m.parameters.map(p => p.parameterType)), dt);
}


