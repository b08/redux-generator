import { TypeId, ClassModel } from "@b08/type-parser";
import { ProductionContext } from "../types/productionContext.type";
import { getReductionClasses } from "../actions/getClassActions";
import { groupBy, KeyGroup } from "@b08/flat-key";
import { memoize } from "@b08/memoize";

export function getReductionClassesByState(state: TypeId, context: ProductionContext): ClassModel[] {
  return getReductionClassesMap(context).get(state) || [];
}

const getReductionClassesMap = memoize(
  function getReductionClassesMap(context: ProductionContext): KeyGroup<TypeId, ClassModel> {
    return groupBy(getReductionClasses(context), r => r.constructor.parameters[0].parameterType.importedTypes[0].id);
  }
);
