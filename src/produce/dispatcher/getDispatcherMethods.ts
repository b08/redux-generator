import { TypeId, FunctionModel, ParameterModel, MethodModel, ClassModel } from "@b08/type-parser";
import { ProductionContext } from "../types/productionContext.type";
import { DispatcherMethod } from "./types";
import { getLoneReductionsByState } from "./getLoneReductionsByState";
import { mapWith } from "@b08/functional";
import { loneActionName, classActionName, camelCase } from "../common/printedLines";
import { flatMap } from "@b08/array";
import { isReductionMethod } from "../actions/getClassActions";
import { getReductionClassesByState } from "./getReductionClassesByState";

export function getLoneDispatcherMethods(state: TypeId, context: ProductionContext): DispatcherMethod[] {
  return mapWith(getLoneDispatcherMethod)(getLoneReductionsByState(state, context), context);
}

function getLoneDispatcherMethod(reduction: FunctionModel, context: ProductionContext): DispatcherMethod {
  const sliced = reduction.parameters.slice(1);
  return {
    name: reduction.id.name,
    actionName: camelCase(loneActionName(reduction, context)),
    parametersLine: parametersLine(sliced),
    actionParametersLine: actionParametersLine(sliced)
  };
}

const parametersLine = (parameters: ParameterModel[]) =>
  parameters.map(p => `${p.parameterName}: ${p.parameterType.typeName}`).join(", ");

const actionParametersLine = (parameters: ParameterModel[]) =>
  parameters.map(p => `, ${p.parameterName}`).join("");

function getClassDispatcherMethod(method: MethodModel, cls: ClassModel, context: ProductionContext): DispatcherMethod {
  return {
    name: method.name,
    actionName: camelCase(classActionName(method, cls, context)),
    parametersLine: parametersLine(method.parameters),
    actionParametersLine: actionParametersLine(method.parameters)
  };
}

export function getClassDispatcherMethods(state: TypeId, context: ProductionContext): DispatcherMethod[] {
  const classes = getReductionClassesByState(state, context);
  return flatMap(classes, cls =>
    cls.methods.filter(m => isReductionMethod(m, cls)).map(m => getClassDispatcherMethod(m, cls, context))
  );
}
