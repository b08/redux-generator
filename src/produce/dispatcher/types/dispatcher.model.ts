import { ImportModel } from "@b08/imports-generator";
import { StateMap } from "../../reducer/types";
import { SelectorStateModel } from "../../selector/types";

export interface DispatchersModel {
  imports: ImportModel[];
  rootState: string;
  actionsImport: string;
  dispatchers: DispatcherModel[];
}

export interface DispatcherModel {
  dispatcherName: string;
  hasStateId: boolean;
  needsStateField: boolean;
  stateId: number;
  stateName: string;
  methods: DispatcherMethod[];
  selector: SelectorStateModel;
  stateMap: StateMap;
}
export interface DispatcherMethod {
  name: string;
  parametersLine: string;
  actionName: string;
  actionParametersLine: string;
}
