import { mapWith } from "@b08/functional";
import { TypeId } from "@b08/type-parser";
import { reduxConstants } from "../../redux.const";
import { ContentFile } from "../../types";
import { allStates } from "../common/allStates";
import { ProductionContext } from "../types/productionContext.type";
import { dispatcher } from "./razor/dispatcher.rzr";
import { groupBy } from "@b08/flat-key";
import { createDispatchersModel } from "./createDispatcherModel";

export function produceDispatchers(context: ProductionContext): ContentFile[] {
  const states = allStates(context).values();
  const grouped = groupBy(states, s => s.folder);
  return mapWith(produceDispatcher)(grouped.values(), context);
}

function produceDispatcher(states: TypeId[], context: ProductionContext): ContentFile {
  return {
    name: reduxConstants.dispatcherFile,
    folder: states[0].folder,
    extension: reduxConstants.tsExtension,
    contents: dispatcher.generate(createDispatchersModel(states, context), context)
  };
}
