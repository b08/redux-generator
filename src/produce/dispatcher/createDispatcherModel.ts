import { mapWith } from "@b08/functional";
import { TypeId } from "@b08/type-parser";
import { getActionsType } from "../actions/getActionType";
import { dispatcherName } from "../common/printedLines";
import { stateHandlesWithId } from "../common/stateHandlesWithId";
import { ProductionContext } from "../types/productionContext.type";
import { dispatcherType } from "./dispatcherType";
import { getClassDispatcherImports, getLoneDispatcherImports } from "./getDispatcherImports";
import { getClassDispatcherMethods, getLoneDispatcherMethods } from "./getDispatcherMethods";
import { DispatcherModel, DispatchersModel } from "./types";
import { getStatePaths } from "../common/statePaths/getStatePaths";
import { getRelativePath, importId, importIds } from "@b08/type-parser-methods";
import { getRootNode } from "../common/getRootNode";
import { createSelectorState } from "../selector/createSelectorModel";
import { getStoreType } from "../storeType/getStoreType";
import { createStateMap } from "../reducer/stateMaps/createStateMap";

export function createDispatchersModel(states: TypeId[], context: ProductionContext): DispatchersModel {
  const dispatchers = createDispatcherModels(states, context);
  const actionsImport = dispatchers.some(d => d.methods.length > 0)
    ? getRelativePath(states[0].folder, getActionsType(context))
    : null;

  const rootNode = getRootNode(context);
  const dType = dispatcherType(states[0]);
  const imports = [
    ...importIds(states, dType),
    importId(getStoreType(context), dType),
    ...getLoneDispatcherImports(states, context),
    ...getClassDispatcherImports(states, context)
  ];
  return {
    actionsImport,
    rootState: rootNode.stateId.name,
    dispatchers,
    imports
  };
}

const createDispatcherModels = mapWith(function getDispatcherModel(state: TypeId, context: ProductionContext): DispatcherModel {
  const methods = [...getLoneDispatcherMethods(state, context), ...getClassDispatcherMethods(state, context)];
  const paths = getStatePaths(state, context);
  const hasStateId = stateHandlesWithId(state, context);
  const selector = createSelectorState(state, context);
  const needsStateField = !hasStateId && (methods.length > 0);
  return {
    hasStateId,
    needsStateField,
    stateId: paths[0].stateId,
    stateName: state.name,
    dispatcherName: dispatcherName(state),
    methods,
    selector,
    stateMap: hasStateId ? createStateMap(state, context) : null
  };
});
