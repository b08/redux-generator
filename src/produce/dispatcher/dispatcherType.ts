import { TypeId } from "@b08/type-parser";
import { reduxConstants } from "../../redux.const";

export function dispatcherType(state: TypeId): TypeId {
  return { ...state, file: reduxConstants.dispatcherFile };
}
