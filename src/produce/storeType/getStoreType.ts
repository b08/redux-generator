import { memoize } from "@b08/memoize";
import { reduxConstants } from "../../redux.const";
import { getRootNode } from "../common/getRootNode";
import { ProductionContext } from "../types/productionContext.type";

export const getStoreType = memoize((context: ProductionContext) => {
  const node = getRootNode(context);
  return {
    file: reduxConstants.storeTypeFile,
    folder: node.stateId.folder,
    name: "IStore",
    isModulesPath: false
  };
});

export const getActionType = memoize((context: ProductionContext) => {
  const node = getRootNode(context);
  return {
    file: reduxConstants.storeTypeFile,
    folder: node.stateId.folder,
    name: "IAction",
    isModulesPath: false
  };
});
