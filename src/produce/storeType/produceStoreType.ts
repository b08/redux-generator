import { reduxConstants } from "../../redux.const";
import { ContentFile } from "../../types";
import { ProductionContext } from "../types/productionContext.type";
import { createStoreTypeModel } from "./createStoreTypeModel";
import { generateStoreType } from "./generator/storeType.gen";
import { getStoreType } from "./getStoreType";

export function produceStoreType(context: ProductionContext): ContentFile {
  const storeType = getStoreType(context);
  const model = createStoreTypeModel(context);
  return {
    folder: storeType.folder,
    name: storeType.file,
    extension: reduxConstants.tsExtension,
    contents: generateStoreType(model, context)
  };

}
