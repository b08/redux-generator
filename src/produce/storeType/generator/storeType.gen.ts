import { generateImports } from "@b08/imports-generator";
import { GeneratorOptions } from "../../../types";
import { StoreTypeModel } from "./storeType.model";

export function generateStoreType(model: StoreTypeModel, options: GeneratorOptions): string {
  return `${generateImports(model.imports, options)}

export interface IAction {
  type: string;
  stateId: number;
}

export type Unsubscribe = () => void;
export type Callback = () => void;

export interface IStore {
  dispatch(action: IAction): void;
  getState(): ${model.rootState};
  subscribe(cb: Callback): Unsubscribe;
}
`;
}
