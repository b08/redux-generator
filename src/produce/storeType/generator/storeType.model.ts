import { ImportModel } from "@b08/imports-generator";

export interface StoreTypeModel {
  imports: ImportModel[];
  rootState: string;
}
