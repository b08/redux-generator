import { getRootNode } from "../common/getRootNode";
import { ProductionContext } from "../types/productionContext.type";
import { StoreTypeModel } from "./generator/storeType.model";
import { getStoreType } from "./getStoreType";
import { importId } from "@b08/type-parser-methods";

export function createStoreTypeModel(context: ProductionContext): StoreTypeModel {
  const store = getStoreType(context);
  const node = getRootNode(context);
  const imports = [importId(node.stateId, store)];
  return {
    imports,
    rootState: node.stateId.name
  };
}
