import { SelectorStateModel } from "./selectorState.model";

export interface SelectorModel {
  selectorName: string;
  rootStateName: string;
  states: SelectorStateModel[];
}
