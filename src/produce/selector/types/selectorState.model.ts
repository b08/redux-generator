export interface SelectorStateModel {
  handlesWithId: boolean;
  mapName: string;
  methodName: string;
  stateName: string;
  cases: SelectorCase[];
  defaultPath: string;
}

export interface SelectorCase {
  stateId: number;
  selectPath: string;
}
