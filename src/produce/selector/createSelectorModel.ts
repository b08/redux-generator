import { TypeId } from "@b08/type-parser";
import { selectorMethodName, stateIdMapName } from "../common/printedLines";
import { stateHandlesWithId } from "../common/stateHandlesWithId";
import { getStatePaths } from "../common/statePaths/getStatePaths";
import { StatePath } from "../common/statePaths/statePath.type";
import { ProductionContext } from "../types/productionContext.type";
import { SelectorCase, SelectorStateModel } from "./types";

export function createSelectorState(state: TypeId, context: ProductionContext): SelectorStateModel {
  const paths = getStatePaths(state, context);
  return {
    methodName: selectorMethodName(state, context),
    mapName: stateIdMapName(state, context),
    handlesWithId: stateHandlesWithId(state, context),
    stateName: state.name,
    cases: paths.map(createSelectorCase),
    defaultPath: paths[0].path.map(p => "." + p).join("")
  };
}

function createSelectorCase(path: StatePath): SelectorCase {
  return {
    stateId: path.stateId,
    selectPath: path.path.map(p => "." + p).join("")
  };
}
