import { SelectorStateModel } from "../types";
import { generator, lines } from "@b08/generator";

export const generateSelectorContent = generator((model: SelectorStateModel) => {
  if (!model.handlesWithId) {
    return `return s${model.defaultPath};`;
  }

  return `switch (this.map[this.stateId]) {
${lines(model.cases, c => `  case ${c.stateId}:
    return s${c.selectPath};`)}
  default: return s${model.defaultPath};
}`;
});
