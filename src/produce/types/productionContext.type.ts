import { ReduxTree, NodeId } from "../../createTree/types";
import { GeneratorOptions, ParsedModel } from "../../types";

export interface ProductionContext extends GeneratorOptions {
  parsed: ParsedModel[];
  tree: ReduxTree;
  root: NodeId;
}
