import { ContentFile } from "../../types";
import { getActionsType } from "./getActionType";
import { ProductionContext } from "../types/productionContext.type";
import { getClassActions, getClassImports } from "./getClassActions";
import { getLoneActions, getLoneImports } from "./getLoneActions";
import { actions } from "./razor/actionsFile.rzr";
import { ActionsModel } from "./types";
import { reduxConstants } from "../../redux.const";
import { getActionType } from "../storeType/getStoreType";
import { importId } from "@b08/type-parser-methods";

export function produceActions(context: ProductionContext): ContentFile {
  const model = createActionsModel(context);
  if (model.actions.length === 0) { return null; }
  const actionType = getActionsType(context);
  return {
    folder: actionType.folder,
    name: actionType.file,
    extension: reduxConstants.tsExtension,
    contents: actions.generate(model, context)
  };
}

function createActionsModel(context: ProductionContext): ActionsModel {
  const loneActions = getLoneActions(context);
  const classActions = getClassActions(context);
  const loneImports = getLoneImports(context);
  const classImports = getClassImports(context);
  const iaction = getActionType(context);
  const actions = getActionsType(context);
  const iactionImport = importId(iaction, actions);
  return {
    imports: [...loneImports, ...classImports, iactionImport],
    actions: [...loneActions, ...classActions]
  };
}
