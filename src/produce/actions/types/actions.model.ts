import { ImportModel } from "@b08/imports-generator";

export interface ActionsModel {
  imports: ImportModel[];
  actions: ActionModel[];
}

export interface ActionModel {
  name: string;
  constantName: string;
  functionName: string;
  constantContent: string;
  parameters: ActionParameter[];
  parametersLine: string;
}

export interface ActionParameter {
  name: string;
  type: string;
}
