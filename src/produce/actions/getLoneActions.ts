import { flatMap } from "@b08/array";
import { memoize } from "@b08/memoize";
import { mapWith } from "@b08/functional";
import { FunctionModel } from "@b08/type-parser";
import { getStateByDefinition } from "../../createTree/stateModels";
import { getActionsType } from "./getActionType";
import { ProductionContext } from "../types/productionContext.type";
import { createLoneAction } from "./createAction";
import { ActionModel } from "./types";
import { allStates } from "../common/allStates";
import { ImportModel } from "@b08/imports-generator";
import { importTypeDefinitions } from "@b08/type-parser-methods";

export const getLoneReductions = memoize(function getLoneReductions(context: ProductionContext): FunctionModel[] {
  return flatMap(context.parsed, p => p.functions)
    .filter(f => isLoneReduction(f, context))
    .sort((r1, r2) => r1.id.name < r2.id.name ? -1 : (r1.id.name > r2.id.name ? 1 : 0));
});

function isLoneReduction(func: FunctionModel, context: ProductionContext): boolean {
  if (func.parameters.length === 0) { return false; }
  if (func.returnType.typeName !== func.parameters[0].parameterType.typeName) { return false; }
  const returnState = getStateByDefinition(func.returnType, context);
  return returnState != null && allStates(context).has(returnState.id);
}

export function getLoneActions(context: ProductionContext): ActionModel[] {
  return mapWith(createLoneAction)(getLoneReductions(context), context);
}

export function getLoneImports(context: ProductionContext): ImportModel[] {
  const at = getActionsType(context);
  const reductions = getLoneReductions(context);
  return importTypeDefinitions(flatMap(reductions, r => r.parameters.slice(1)).map(p => p.parameterType), at);
}
