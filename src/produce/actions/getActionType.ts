import { TypeId } from "@b08/type-parser";
import { reduxConstants } from "../../redux.const";
import { getRootNode } from "../common/getRootNode";
import { ProductionContext } from "../types/productionContext.type";

export function getActionsType(context: ProductionContext): TypeId {
  const node = getRootNode(context);
  return {
    file: reduxConstants.actionsFile,
    folder: node.stateId.folder,
    name: reduxConstants.actionsFile,
    isModulesPath: false
  };
}
