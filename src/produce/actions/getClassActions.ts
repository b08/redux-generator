import { flatMap } from "@b08/array";
import { memoize } from "@b08/memoize";
import { ImportModel } from "@b08/imports-generator";
import { ClassModel, MethodModel } from "@b08/type-parser";
import { getStateByDefinition } from "../../createTree/stateModels";
import { allStates } from "../common/allStates";
import { getActionsType } from "./getActionType";
import { ProductionContext } from "../types/productionContext.type";
import { createClassAction } from "./createAction";
import { ActionModel } from "./types";
import { importTypeDefinitions } from "@b08/type-parser-methods";

export const getReductionClasses = memoize(function getReductionClasses(context: ProductionContext): ClassModel[] {
  return flatMap(context.parsed, p => p.classes)
    .filter(c => isReductionClass(c, context))
    .sort((c1, c2) => c1.id.name < c2.id.name ? -1 : (c1.id.name > c2.id.name ? 1 : 0));
});

function isReductionClass(cls: ClassModel, context: ProductionContext): boolean {
  if (cls.constructor == null || cls.constructor.parameters.length !== 1
    || cls.extends.length > 0 || cls.implements.length > 0 || cls.fields.length > 0) { return false; }
  const constructorState = getStateByDefinition(cls.constructor.parameters[0].parameterType, context);
  return constructorState != null && allStates(context).has(constructorState.id);
}

export function getClassActions(context: ProductionContext): ActionModel[] {
  return flatMap(getReductionClasses(context), cls =>
    cls.methods.filter(m => isReductionMethod(m, cls)).map(m => createClassAction(m, cls, context))
  );
}

export function isReductionMethod(method: MethodModel, cls: ClassModel): boolean {
  const stateType = cls.constructor.parameters[0].parameterType.typeName;
  return method.returnType.typeName === cls.constructor.parameters[0].parameterType.typeName
    && !method.parameters.some(p => p.parameterType.typeName === stateType);
}

export function getClassImports(context: ProductionContext): ImportModel[] {
  const at = getActionsType(context);
  const classes = getReductionClasses(context);
  const methods = flatMap(classes, cls => cls.methods.filter(m => isReductionMethod(m, cls)));
  return importTypeDefinitions(flatMap(methods, m => m.parameters.map(p => p.parameterType)), at);
}
