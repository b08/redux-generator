// warning! This code was generated by @b08/razor
// manual changes to this file will be overwritten next time the code is regenerated.
import { Generator, GeneratorOptions } from "@b08/generator";
import { ActionsModel } from "../types";
import { imports } from "@b08/imports-generator";

function generateContent(model: ActionsModel, gen: Generator): Generator {
  gen = gen.withIndent(``, g => imports.generateContent(model.imports, g));
  gen = gen.eol();
  for (const action of model.actions) {
    gen = gen.append(`export const `);
    gen = gen.append((action.constantName).toString());
    gen = gen.append(` = `);
    gen = gen.quote();
    gen = gen.append((action.constantContent).toString());
    gen = gen.quote();
    gen = gen.append(`;`);
    gen = gen.eol();
    gen = gen.append(`export interface `);
    gen = gen.append((action.name).toString());
    gen = gen.append(` extends IAction {`);
    gen = gen.eol();
    for (let parm of action.parameters.slice(1)) {
      gen = gen.append(`  `);
      gen = gen.append((parm.name).toString());
      gen = gen.append(`: `);
      gen = gen.append((parm.type).toString());
      gen = gen.append(`;`);
      gen = gen.eol();
    }
    gen = gen.append(`}`);
    gen = gen.eol();
    gen = gen.append(`export function `);
    gen = gen.append((action.functionName).toString());
    gen = gen.append(`(`);
    gen = gen.append((action.parametersLine).toString());
    gen = gen.append(`): `);
    gen = gen.append((action.name).toString());
    gen = gen.append(` {`);
    gen = gen.eol();
    gen = gen.append(`  return {`);
    gen = gen.eol();
    gen = gen.append(`    type: `);
    gen = gen.append((action.constantName).toString());
    gen = gen.append(`,`);
    gen = gen.eol();
    for (let parm of action.parameters) {
      gen = gen.append(`    `);
      gen = gen.append((parm.name).toString());
      gen = gen.append(`,`);
      gen = gen.eol();
    }
    gen = gen.append(`  };`);
    gen = gen.eol();
    gen = gen.append(`}`);
    gen = gen.eol();
    gen = gen.eol();
  }
  gen = gen.append(`export const actions = {`);
  gen = gen.eol();
  for (const action of model.actions) {
    gen = gen.append(`  `);
    gen = gen.append((action.constantName).toString());
    gen = gen.append(`,`);
    gen = gen.eol();
    gen = gen.append(`  `);
    gen = gen.append((action.functionName).toString());
    gen = gen.append(`,`);
    gen = gen.eol();
  }
  gen = gen.append(`};`);
  gen = gen.eol();
  return gen;
}

function generate(model: ActionsModel, options: GeneratorOptions): string {
  return generateContent(model, new Generator(options)).toString();
}

export const actions = {
  generate,
  generateContent
};
