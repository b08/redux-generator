import { ClassModel, FunctionModel, MethodModel, ParameterModel } from "@b08/type-parser";
import { classActionName, classConstantName, loneActionName, loneConstantName, loneConstantContent, classConstantContent, camelCase } from "../common/printedLines";
import { stateIdParameter } from "../common/stateIdType";
import { ActionModel, ActionParameter } from "./types";
import { ProductionContext } from "../types/productionContext.type";

export function createLoneAction(reduction: FunctionModel, context: ProductionContext): ActionModel {
  return {
    name: loneActionName(reduction, context),
    functionName: camelCase(loneActionName(reduction, context)),
    constantName: loneConstantName(reduction, context),
    constantContent: loneConstantContent(reduction),
    parametersLine: parametersLine(reduction.parameters.slice(1)),
    parameters: parameters(reduction.parameters.slice(1))
  };
}

export function createClassAction(method: MethodModel, cls: ClassModel, context: ProductionContext): ActionModel {
  return {
    name: classActionName(method, cls, context),
    functionName: camelCase(classActionName(method, cls, context)),
    constantName: classConstantName(method, cls, context),
    constantContent: classConstantContent(method, cls),
    parametersLine: parametersLine(method.parameters),
    parameters: parameters(method.parameters),
  };
}

function parametersLine(parameters: ParameterModel[]): string {
  return [stateIdParameter, ...parameters].map(parameterLine).join(", ");
}

function parameters(parameters: ParameterModel[]): ActionParameter[] {
  return [stateIdParameter, ...parameters].map(parameter);
}

function parameter(p: ParameterModel): ActionParameter {
  return {
    name: p.parameterName,
    type: p.parameterType.typeName
  };
}

function parameterLine(parameter: ParameterModel): string {
  return `${parameter.parameterName}: ${parameter.parameterType.typeName}`;
}
