import { TypeId } from "@b08/type-parser";
import { isReductionMethod } from "../actions/getClassActions";
import { allStates } from "../common/allStates";
import { getLoneReductionsByState } from "../dispatcher/getLoneReductionsByState";
import { getReductionClassesByState } from "../dispatcher/getReductionClassesByState";
import { ProductionContext } from "../types/productionContext.type";

export function hasActions(state: TypeId, context: ProductionContext): boolean {
  const lone = getLoneReductionsByState(state, context).length;
  if (lone > 0) {
    return true;
  }
  const cls = getReductionClassesByState(state, context)
    .filter(cls => cls.methods.some(m => isReductionMethod(m, cls))).length;
  return cls > 0;
}

export function statesWithActions(context: ProductionContext): TypeId[] {
  return allStates(context).values()
    .filter(s => hasActions(s, context));
}
