import { groupBy } from "@b08/flat-key";
import { IMap } from "@b08/object-map";
import { ReductionCall } from "./types";

export function createCase(path: string, paths: string[][]): ReductionCall {
  if (paths[0].length === 0) { // paths.length === 1 &&
    return {
      isAssignment: true,
      path: path,
      fields: null
    };
  }

  const grouped = groupBy(paths, p => p[0]);
  const fields: IMap<ReductionCall> = {};
  for (let field of grouped.keys()) {
    fields[field] = createCase(`${path}.${field}`, grouped.get(field).map(p => p.slice(1)));
  }
  return {
    path,
    fields,
    isAssignment: false
  };
}
