import { TypeId } from "@b08/type-parser";
import { NodeId, ReduxTree } from "../../../createTree/types";
import { stateIdMapName } from "../../common/printedLines";
import { getStatePaths } from "../../common/statePaths/getStatePaths";
import { StatePath } from "../../common/statePaths/statePath.type";
import { ProductionContext } from "../../types/productionContext.type";
import { StateMap } from "../types";
import { getNode } from "../../../createTree/getNode";
import { flatMap } from "@b08/array";

export function createStateMap(state: TypeId, context: ProductionContext): StateMap {
  const paths = getStatePaths(state, context)
    .sort((p1, p2) => p1.path.length - p2.path.length);
  const idMaps = getIdMaps(context.root, paths, 0, context.tree)
    .sort((m1, m2) => m1.stateId - m2.stateId);
  return {
    mapName: stateIdMapName(state, context),
    mapValues: idMaps.map(i => `, ${i.mapsTo}`).join("")
  };
}

interface IdMap {
  stateId: number;
  mapsTo: number;
}

function getIdMaps(id: NodeId, paths: StatePath[], parentId: number, tree: ReduxTree): IdMap[] {
  const node = getNode(id, tree);
  const mapsTo = paths.length > 0 ? paths[0].stateId : parentId;
  const children = flatMap(node.fields, field => getIdMaps(field.nodeId, fieldPaths(paths, field.fieldName), mapsTo, tree));
  return [{ stateId: node.id.nodeId, mapsTo }, ...children];
}

function fieldPaths(paths: StatePath[], fieldName: string): StatePath[] {
  return paths.filter(p => p.path[0] === fieldName)
    .map(p => ({ stateId: p.stateId, path: p.path.slice(1) }));
}
