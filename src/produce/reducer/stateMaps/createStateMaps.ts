import { TypeId } from "@b08/type-parser";
import { allStates } from "../../common/allStates";
import { stateHandlesWithId } from "../../common/stateHandlesWithId";
import { getLoneReductionsByState } from "../../dispatcher/getLoneReductionsByState";
import { getReductionClassesByState } from "../../dispatcher/getReductionClassesByState";
import { ProductionContext } from "../../types/productionContext.type";
import { StateMap } from "../types";
import { createStateMap } from "./createStateMap";

export function createStateMaps(context: ProductionContext): StateMap[] {
  return allStates(context).values()
    .filter(s => stateHandlesWithId(s, context) && hasReductions(s, context))
    .map(s => createStateMap(s, context));
}

function hasReductions(state: TypeId, context: ProductionContext): boolean {
  return getLoneReductionsByState(state, context).length +
    getReductionClassesByState(state, context).length
    > 0;
}


