import { ProductionContext } from "../../types/productionContext.type";
import { InitializerModel, InitializerChild } from "../types";
import { getRootNode } from "../../common/getRootNode";
import { NodeField } from "../../../createTree/types";
import { mapWith } from "@b08/functional";
import { getNode } from "../../../createTree/getNode";
import { createInitializerFields } from "./createInitializerFields";
import { initializerName } from "../../common/printedLines";

export function createInitializerModel(context: ProductionContext): InitializerModel {
  const root = getRootNode(context);
  return {
    rootStateName: root.stateId.name,
    initializerName: initializerName(root.stateId.name),
    fields: createInitializerFields(root, context),
    children: createInitilaizerChildren(root.fields, context)
  };
}

const createInitilaizerChildren = mapWith(function createInitilaizerChildr
  (field: NodeField, context: ProductionContext): InitializerChild {
  const node = getNode(field.nodeId, context.tree);
  return {
    fieldName: field.fieldName,
    fields: createInitializerFields(node, context),
    children: createInitilaizerChildren(node.fields, context)
  };
});
