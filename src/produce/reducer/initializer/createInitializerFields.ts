import { ProductionContext } from "../../types/productionContext.type";
import { InitializerField } from "../types";
import { ReduxNode } from "../../../createTree/types";
import { getStateById } from "../../../createTree/stateModels";
import { getNonStateFields } from "../../../createTree/isStateField";
import { mapWith } from "@b08/functional";
import { FieldModel } from "@b08/type-parser";
import { isStateId } from "../../common/stateHandlesWithId";
import { reduxConstants } from "../../../redux.const";

export function createInitializerFields(node: ReduxNode, context: ProductionContext): InitializerField[] {
  const state = getStateById(node.stateId, context);
  const fields = getNonStateFields(state.fields, context);
  const valueFields = fields.filter(f => !isStateId(f));
  const initializedFields = initializerFields(valueFields);
  const stateIdField = { fieldName: reduxConstants.stateIdField, fieldValue: `${node.id.nodeId}` };
  return [stateIdField, ...initializedFields];
}

const initializerFields = mapWith(function initializerField(field: FieldModel): InitializerField {
  return {
    fieldName: field.fieldName,
    fieldValue: getFieldValue(field.fieldType.typeName)
  };
});

function getFieldValue(type: string): string {
  if (type === "boolean") { return "false"; }
  if (type === "number") { return "0"; }
  if (type.endsWith("[]")) { return "[]"; }

  return "null";
}
