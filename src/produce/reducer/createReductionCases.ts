import { IMap } from "@b08/object-map";
import { TypeId } from "@b08/type-parser";
import { ProductionContext } from "../types/productionContext.type";
import { getStatePaths } from "../common/statePaths/getStatePaths";
import { ReductionCall } from "./types";
import { createCase } from "./createCase";
import { stateHandlesWithId } from "../common/stateHandlesWithId";

export function createReductionCases(state: TypeId, context: ProductionContext): IMap<ReductionCall> {
  if (!stateHandlesWithId(state, context)) { return null; }

  const paths = getStatePaths(state, context);
  const cases: IMap<ReductionCall> = {};
  paths.forEach(p => cases[p.stateId] = createCase("", [p.path]));
  return cases;
}

export function createDefaultCase(state: TypeId, context: ProductionContext): ReductionCall {
  const paths = getStatePaths(state, context);
  return createCase("", paths.map(p => p.path));
}


