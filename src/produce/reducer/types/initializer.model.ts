export interface InitializerModel {
  rootStateName: string;
  initializerName: string;
  fields: InitializerField[];
  children: InitializerChild[];
}

export interface InitializerChild {
  fieldName: string;
  fields: InitializerField[];
  children: InitializerChild[];
}

export interface InitializerField {
  fieldName: string;
  fieldValue: string;
}
