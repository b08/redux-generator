import { IMap } from "@b08/object-map";

export interface ReductionCall {
  path: string;
  isAssignment: boolean;
  fields: IMap<ReductionCall>;
}
