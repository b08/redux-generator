import { IMap } from "@b08/object-map";
import { ReductionCall } from "./reductionCall";

export interface StateMethodModel {
  handlesWithId: boolean;
  mapName: string;
  stateName: string;
  methodName: string;
  cases: IMap<ReductionCall>;
  defaultCase: ReductionCall;
}
