import { ImportModel } from "@b08/imports-generator";
import { InitializerModel } from "./initializer.model";
import { StateMap } from "./stateMap.type";
import { StateMethodModel } from "./stateMethod.model";

export interface ReducerModel {
  actionsPath: string;
  rootStateName: string;
  reducerName: string;
  imports: ImportModel[];
  initializer: InitializerModel;
  actions: ReducerAction[];
  stateMethods: StateMethodModel[];
  stateMaps: StateMap[];
}

export interface ReducerAction {
  castAction: boolean;
  actionName: string;
  actionVar: string;
  constantName: string;
  stateCall: string;
}


