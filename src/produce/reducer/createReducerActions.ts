import { flatMap } from "@b08/array";
import { memoize } from "@b08/memoize";
import { ClassModel, FunctionModel, MethodModel } from "@b08/type-parser";
import { getReductionClasses, isReductionMethod } from "../actions/getClassActions";
import { getLoneReductions } from "../actions/getLoneActions";
import { classActionName, classConstantName, classActionVar, loneActionName, loneConstantName, loneActionVar } from "../common/printedLines";
import { classReductionStateCall, loneReductionStateCall } from "../common/stateCalls";
import { ProductionContext } from "../types/productionContext.type";
import { ReducerAction } from "./types";

export const createReducerActions = memoize(function (context: ProductionContext): ReducerAction[] {
  const loneActions = getLoneReductions(context).map(r => createLoneAction(r, context));
  const classActions = flatMap(getReductionClasses(context), cls =>
    cls.methods.filter(m => isReductionMethod(m, cls)).map(m => createClassAction(m, cls, context)));
  return [...loneActions, ...classActions];
});

function createLoneAction(reduction: FunctionModel, context: ProductionContext): ReducerAction {
  return {
    castAction: reduction.parameters.length > 1,
    actionName: loneActionName(reduction, context),
    actionVar: loneActionVar(reduction, context),
    constantName: loneConstantName(reduction, context),
    stateCall: loneReductionStateCall(reduction, context)
  };
}


function createClassAction(method: MethodModel, cls: ClassModel, context: ProductionContext): ReducerAction {
  return {
    castAction: method.parameters.length > 0,
    actionName: classActionName(method, cls, context),
    actionVar: classActionVar(method, cls, context),
    constantName: classConstantName(method, cls, context),
    stateCall: classReductionStateCall(method, cls, context)
  };
}
