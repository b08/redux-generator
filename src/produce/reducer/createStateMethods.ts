import { TypeId } from "@b08/type-parser";
import { stateIdMethodName, stateIdMapName } from "../common/printedLines";
import { stateHandlesWithId } from "../common/stateHandlesWithId";
import { ProductionContext } from "../types/productionContext.type";
import { statesWithActions } from "./statesWithActions";
import { createDefaultCase, createReductionCases } from "./createReductionCases";
import { StateMethodModel } from "./types";

export function createStateMethods(context: ProductionContext): StateMethodModel[] {
  return statesWithActions(context)
    .map(s => createStateMethod(s, context));
}

function createStateMethod(state: TypeId, context: ProductionContext): StateMethodModel {
  return {
    methodName: stateIdMethodName(state, context),
    mapName: stateIdMapName(state, context),
    stateName: state.name,
    handlesWithId: stateHandlesWithId(state, context),
    cases: createReductionCases(state, context),
    defaultCase: createDefaultCase(state, context)
  };
}
