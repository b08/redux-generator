import { ImportModel } from "@b08/imports-generator";
import { ClassModel, FunctionModel } from "@b08/type-parser";
import { getRelativePath, importId, importIds } from "@b08/type-parser-methods";
import { getReductionClasses } from "../actions/getClassActions";
import { getLoneReductions } from "../actions/getLoneActions";
import { allStates } from "../common/allStates";
import { getRootNode } from "../common/getRootNode";
import { loneReductionAlias, reductionClassAlias } from "../common/printedLines";
import { getActionType } from "../storeType/getStoreType";
import { ProductionContext } from "../types/productionContext.type";
import { getReducerType } from "./getReducerType";
import { hasActions } from "./statesWithActions";
import { createReducerActions } from "./createReducerActions";
import { getActionsType } from "../actions/getActionType";

export function createReductionImports(context: ProductionContext): ImportModel[] {
  const rootNode = getRootNode(context);
  const loneImports = getLoneReductions(context).map(reduction => createLoneImport(reduction, context));
  const classImports = getReductionClasses(context).map(cls => createClassImport(cls, context));
  const stateImports = getStateImports(context);
  const actionImports = getActionImports(context);
  return [
    importId(rootNode.stateId, getReducerType(context)),
    importId(getActionType(context), getReducerType(context)),
    ...loneImports,
    ...classImports,
    ...stateImports,
    ...actionImports
  ];
}

function getActionImports(context: ProductionContext): ImportModel[] {
  const reducerType = getReducerType(context);
  const actions = createReducerActions(context);
  const type = getActionsType(context);
  const types = actions.filter(a => a.castAction).map(a => ({ ...type, name: a.actionName }));
  return importIds(types, reducerType);
}

function getStateImports(context: ProductionContext): ImportModel[] {
  const reducerType = getReducerType(context);
  const states = allStates(context).values()
    .filter(s => hasActions(s, context)); // || stateHandlesWithId(s, context));
  return importIds(states, reducerType);
}

function createLoneImport(reduction: FunctionModel, context: ProductionContext): ImportModel {
  return {
    type: reduction.id.name,
    alias: loneReductionAlias(reduction, context),
    importPath: getRelativePath(getReducerType(context).folder, reduction.id)
  };
}

function createClassImport(cls: ClassModel, context: ProductionContext): ImportModel {
  return {
    alias: reductionClassAlias(cls, context),
    importPath: getRelativePath(getReducerType(context).folder, cls.id),
    type: cls.id.name
  };
}
