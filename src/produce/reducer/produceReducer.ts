import { getRelativePath } from "@b08/type-parser-methods";
import { reduxConstants } from "../../redux.const";
import { ContentFile } from "../../types";
import { getActionsType } from "../actions/getActionType";
import { getRootNode } from "../common/getRootNode";
import { reducerName } from "../common/printedLines";
import { ProductionContext } from "../types/productionContext.type";
import { createReducerActions } from "./createReducerActions";
import { createReductionImports } from "./createReductionImports";
import { createStateMethods } from "./createStateMethods";
import { getReducerType } from "./getReducerType";
import { createInitializerModel } from "./initializer/createInitializerModel";
import { reducer } from "./razor/reducer.rzr";
import { createStateMaps } from "./stateMaps/createStateMaps";
import { ReducerModel } from "./types";

export function produceReducer(context: ProductionContext): ContentFile {
  const reducerType = getReducerType(context);
  const node = getRootNode(context);
  const model: ReducerModel = {
    actionsPath: getRelativePath(reducerType.folder, getActionsType(context)),
    rootStateName: node.stateId.name,
    reducerName: reducerName(node.stateId.name),
    imports: createReductionImports(context),
    initializer: createInitializerModel(context),
    actions: createReducerActions(context),
    stateMethods: createStateMethods(context),
    stateMaps: createStateMaps(context)
  };

  return {
    folder: reducerType.folder,
    name: reducerType.file,
    extension: reduxConstants.tsExtension,
    contents: reducer.generate(model, context)
  };
}
