import { TypeId } from "@b08/type-parser";
import { reduxConstants } from "../../redux.const";
import { getRootNode } from "../common/getRootNode";
import { ProductionContext } from "../types/productionContext.type";
import { memoize } from "@b08/memoize";

export const getReducerType = memoize(function getReducerType(context: ProductionContext): TypeId {
  const node = getRootNode(context);
  return {
    file: reduxConstants.reducerFile,
    folder: node.stateId.folder,
    name: reduxConstants.reducerFile,
    isModulesPath: false
  };
});
