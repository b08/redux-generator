import { ContentFile } from "../types";
import { produceActions } from "./actions/produceActions";
import { produceDispatchers } from "./dispatcher/produceDispatchers";
import { produceReducer } from "./reducer/produceReducer";
import { produceStoreType } from "./storeType/produceStoreType";
import { ProductionContext } from "./types/productionContext.type";

export function produceFiles(context: ProductionContext): ContentFile[] {
  const actions = produceActions(context);
  const reducer = produceReducer(context);
  const dispatchers = produceDispatchers(context);
  const storeType = produceStoreType(context);
  return [actions, reducer, storeType , ...dispatchers];
}


