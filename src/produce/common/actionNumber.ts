import { FunctionModel, TypeId, MethodModel, ClassModel } from "@b08/type-parser";
import { ProductionContext } from "../types/productionContext.type";
import { memoize } from "@b08/memoize";
import { KeyMap } from "@b08/flat-key";
import { getLoneReductions } from "../actions/getLoneActions";
import { IMap } from "@b08/object-map";
import { getReductionClasses, isReductionMethod } from "../actions/getClassActions";
import { flatMap } from "@b08/array";

export function actionNumber(reduction: FunctionModel, context: ProductionContext): number {
  return actionNumberMap(context).get(reduction.id);
}

const actionNumberMap = memoize(function actionNumberMap(context: ProductionContext): KeyMap<TypeId, number> {
  const result = new KeyMap<TypeId, number>();
  getLoneReductions(context).forEach((r, i) => result.add(r.id, i));
  return result;
});

export function classActionNumber(method: MethodModel, cls: ClassModel, context: ProductionContext): number {
  return classActionNumberMap(context).get(cls.id)[method.name];
}

const classActionNumberMap = memoize(function actionNumberMap(context: ProductionContext): KeyMap<TypeId, IMap<number>> {
  const start = getLoneReductions(context).length;
  const pairs = flatMap(getReductionClasses(context),
    (cls) => cls.methods.filter(m => isReductionMethod(m, cls)).map(m => ({ cls, method: m.name })));
  const result = new KeyMap<TypeId, IMap<number>>();
  pairs.forEach((p, i) => {
    if (!result.has(p.cls.id)) { result.add(p.cls.id, {}); }
    result.get(p.cls.id)[p.method] = i + start;
  });
  return result;
});
