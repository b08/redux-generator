import { memoize } from "@b08/memoize";
import { KeyMap } from "@b08/flat-key";
import { TypeId } from "@b08/type-parser";
import { ProductionContext } from "../types/productionContext.type";
import { allStates } from "./allStates";

export function stateIdNumber(state: TypeId, context: ProductionContext): number {
  return stateNumberMap(context).get(state);
}

const stateNumberMap = memoize(function stateNumberMap(context: ProductionContext): KeyMap<TypeId, number> {
  const result = new KeyMap<TypeId, number>();
  allStates(context).values().forEach((s, i) => result.add(s, i));
  return result;
});
