import { ClassModel, TypeId } from "@b08/type-parser";
import { ProductionContext } from "../types/productionContext.type";
import { getReductionClasses } from "../actions/getClassActions";
import { KeyMap } from "@b08/flat-key";
import { memoize } from "@b08/memoize";

export function reductionClassNumber(cls: ClassModel, context: ProductionContext): number {
  return reductionClassNumberMap(context).get(cls.id);
}

const reductionClassNumberMap = memoize(function reductionClassNumberMap(context: ProductionContext): KeyMap<TypeId, number> {
  const result = new KeyMap<TypeId, number>();
  getReductionClasses(context).forEach((c, i) => result.add(c.id, i));
  return result;
});
