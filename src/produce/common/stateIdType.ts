import { ParameterModel } from "@b08/type-parser";
import { reduxConstants } from "../../redux.const";

export const stateIdParameter: ParameterModel = {
  parameterName: reduxConstants.stateIdField,
  isOptional: false,
  parameterType: { typeName: reduxConstants.stateIdType, importedTypes: [] }
};
