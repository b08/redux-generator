import { FunctionModel, ClassModel, MethodModel } from "@b08/type-parser";
import { ProductionContext } from "../types/productionContext.type";
import { loneReductionAlias, reductionClassAlias, stateMethodName, loneActionVar, classActionVar } from "./printedLines";
import { stateHandlesWithId } from "./stateHandlesWithId";

export function loneReductionStateCall(reduction: FunctionModel, context: ProductionContext): string {
  const actionVar = loneActionVar(reduction, context);
  const parameters = reduction.parameters.slice(1).map(p => `, ${actionVar}.${p.parameterName}`).join("");
  const stateMethod = stateMethodName(reduction.returnType, context);
  const handlesWithId = stateHandlesWithId(reduction.returnType.importedTypes[0].id, context) ? "a.stateId, " : "";
  return `${stateMethod}(p, ${handlesWithId}s => ${loneReductionAlias(reduction, context)}(s${parameters}))`;
}

export function classReductionStateCall(method: MethodModel, cls: ClassModel, context: ProductionContext): string {
  const actionVar = classActionVar(method, cls, context);
  const parameters = method.parameters.map(p => `${actionVar}.${p.parameterName}`).join(", ");
  const stateMethod = stateMethodName(method.returnType, context);
  const handlesWithId = stateHandlesWithId(method.returnType.importedTypes[0].id, context) ? "a.stateId, " : "";
  return `${stateMethod}(p, ${handlesWithId}s => new ${reductionClassAlias(cls, context)}(s).${method.name}(${parameters}))`;
}
