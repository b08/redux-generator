import { ProductionContext } from "../types/productionContext.type";
import { ReduxNode } from "../../createTree/types";
import { getNode } from "../../createTree/getNode";

export function getRootNode(context: ProductionContext): ReduxNode {
  return getNode(context.root, context.tree);
}
