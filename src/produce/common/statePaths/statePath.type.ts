export interface StatePath {
  stateId: number;
  path: string[];
}
