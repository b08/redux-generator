import { memoize } from "@b08/memoize";
import { KeyGroup } from "@b08/flat-key";
import { TypeId } from "@b08/type-parser";
import { NodeId, ReduxTree } from "../../../createTree/types";
import { ProductionContext } from "../../types/productionContext.type";
import { StatePath } from "./statePath.type";
import { getNode } from "../../../createTree/getNode";

export function getStatePaths(state: TypeId, context: ProductionContext): StatePath[] {
  return getAllPaths(context.root, context.tree).get(state);
}

const getAllPaths = memoize(function getAllPaths(root: NodeId, tree: ReduxTree): KeyGroup<TypeId, StatePath> {
  const group = new KeyGroup<TypeId, StatePath>();
  addNode([], root, tree, group);
  return group;
});

function addNode(path: string[], id: NodeId, tree: ReduxTree, grp: KeyGroup<TypeId, StatePath>): void {
  const node = getNode(id, tree);
  grp.add(node.stateId, { stateId: node.id.nodeId, path });
  node.fields.forEach(field => addNode([...path, field.fieldName], field.nodeId, tree, grp));
}
