import { ClassModel, FunctionModel, MethodModel, TypeDefinition, TypeId } from "@b08/type-parser";
import { camelCase } from "change-case";
import { reduxConstants } from "../../redux.const";
import { ProductionContext } from "../types/productionContext.type";
import { actionNumber, classActionNumber } from "./actionNumber";
import { reductionClassNumber } from "./reductionClassNumber";
import { stateIdNumber } from "./stateNumber";
export { camelCase } from "change-case";

export const loneActionName = (reduction: FunctionModel, context: ProductionContext) =>
  `A${actionNumber(reduction, context)}`;
export const loneConstantName = (reduction: FunctionModel, context: ProductionContext) =>
  `T${actionNumber(reduction, context)}`;
export const loneConstantContent = (reduction: FunctionModel) =>
  `${reduction.returnType.typeName} - ${reduction.id.name}`;


export const classActionName = (method: MethodModel, cls: ClassModel, context: ProductionContext) =>
  `A${classActionNumber(method, cls, context)}`;
export const classConstantName = (method: MethodModel, cls: ClassModel, context: ProductionContext) =>
  `T${classActionNumber(method, cls, context)}`;
export const classConstantContent = (method: MethodModel, cls: ClassModel) =>
  `${method.returnType.typeName} - ${cls.id.name} - ${method.name}`;


export const loneActionVar = (reduction: FunctionModel, context: ProductionContext) =>
  `a${actionNumber(reduction, context)}`;
export const classActionVar = (method: MethodModel, cls: ClassModel, context: ProductionContext) =>
  `a${classActionNumber(method, cls, context)}`;

export const reducerName = (stateName: string) =>
  camelCase(stripStateName(stateName)) + reduxConstants.reducerSuffix;

export const initializerName = (stateName: string) =>
  reduxConstants.initializerPrefix + stripStateName(stateName);

const stateNameRegex = new RegExp(`(.*)${reduxConstants.stateSuffix}$`);
export function stripStateName(stateName: string): string {
  const match = stateName.match(stateNameRegex);
  return match && match[1];
}

export const loneReductionAlias = (reduction: FunctionModel, context: ProductionContext) =>
  `r${actionNumber(reduction, context)}`;

export const reductionClassAlias = (cls: ClassModel, context: ProductionContext) =>
  `C${reductionClassNumber(cls, context)}`;

export const dispatcherName = (state: TypeId) =>
  stripStateName(state.name) + reduxConstants.dispatcherSuffix;

export const selectorName = (state: TypeId) =>
  camelCase(stripStateName(state.name)) + reduxConstants.selectorSuffix;

export const stateIdMethodName = (state: TypeId, context: ProductionContext) =>
  `m${stateIdNumber(state, context)}`;

export const stateIdMapName = (state: TypeId, context: ProductionContext) =>
  `n${stateIdNumber(state, context)}`;

export const stateMethodName = (state: TypeDefinition, context: ProductionContext) =>
  stateIdMethodName(state.importedTypes[0].id, context);

export const selectorMethodName = (state: TypeId, context: ProductionContext) =>
  `s${stateIdNumber(state, context)}`;
