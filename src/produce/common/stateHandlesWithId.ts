import { TypeId, InterfaceModel, FieldModel } from "@b08/type-parser";
import { ProductionContext } from "../types/productionContext.type";
import { getStateById } from "../../createTree/stateModels";
import { memoize } from "@b08/memoize";
import { reduxConstants } from "../../redux.const";
import { getStatePaths } from "./statePaths/getStatePaths";

export function stateHandlesWithId(id: TypeId, context: ProductionContext): boolean {
  return hasId(getStateById(id, context)) && getStatePaths(id, context).length > 1;
}

const hasId = memoize(function hasId(state: InterfaceModel): boolean {
  return state.fields.some(isStateId);
});

export function isStateId(field: FieldModel): boolean {
  return field.fieldName === reduxConstants.stateIdField
    && field.fieldType.typeName === reduxConstants.stateIdType;
}
