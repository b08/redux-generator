import { ProductionContext } from "../types/productionContext.type";
import { memoize } from "@b08/memoize";
import { NodeId, ReduxTree, ReduxNode } from "../../createTree/types";
import { TypeId } from "@b08/type-parser";
import { KeySet } from "@b08/flat-key";
import { getNode } from "../../createTree/getNode";
import { flatMap } from "@b08/array";

export function allStates(context: ProductionContext): KeySet<TypeId> {
  return allStatesInTree(context.root, context.tree);
}

const allStatesInTree = memoize(function allStatesInTree(root: NodeId, tree: ReduxTree): KeySet<TypeId> {
  const allNodes = allNodesInTree(root, tree).map(node => node.stateId);
  return new KeySet(allNodes);
});

const allNodesInTree = memoize(getTreeNodes);

function getTreeNodes(nodeId: NodeId, tree: ReduxTree): ReduxNode[] {
  const node = getNode(nodeId, tree);
  const children = flatMap(node.fields, f => getTreeNodes(f.nodeId, tree));
  return [node, ...children];
}
