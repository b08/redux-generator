import { flatMap } from "@b08/array";
import { mapWith } from "@b08/functional";
import { parseTypesInFiles } from "@b08/type-parser";
import { createTree } from "./createTree/createTree";
import { getNode } from "./createTree/getNode";
import { TreeContext } from "./createTree/types";
import { produceFiles } from "./produce/produceFiles";
import { ContentFile, GeneratorOptions } from "./types";
import { disclaimer } from "./disclaimer/disclaimer";

const defaultOptions: GeneratorOptions = {
  linefeed: "\n",
  quotes: "\"",
  aliasMap: {}
};

export function generate<T extends ContentFile>(files: T[], options: GeneratorOptions = {}): T[] {
  options = { ...defaultOptions, ...options };
  const parsed = parseTypesInFiles(files, options);
  const context: TreeContext = { parsed };
  const tree = createTree(context);
  if (tree.roots.length > 1) { // might want to rethink that in future, so far it is the limitation
    const states = mapWith(getNode)(tree.roots, tree).map(n => n.stateId.name).join(", ");
    throw new Error(`Should only be one root node: ${states}`);
  }
  const resultFiles = flatMap(tree.roots, root => {
    const productionContext = { ...options, parsed, tree, root };
    return produceFiles(productionContext);
  });
  return resultFiles.filter(x => x != null).
    map(r => ({ ...files[0], ...r, contents: disclaimer(options) + r.contents }));
}

// function normalizePath(path: string): string {
//   return resolve(path).replace(/[\\\/]+/, "/");
// }
