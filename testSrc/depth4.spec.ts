import { test } from "@b08/test-runner";
import { getDispatcher, getStore } from "./testStore";

test("depth4, should run reduction", async expect => {
  // arrange
  const store = await getStore("../testData/depth4");
  const dispatcher = await getDispatcher("../testData/depth4/subFolder", f => f.FourthDispatcher, store);

  // act
  dispatcher.flipFlag();
  const result1 = store.getState();
  dispatcher.flipFlag();
  const result2 = store.getState();
  const selectedState = dispatcher.getState();

  // assert
  expect.equal(result1.second.third.fourth.flag, true);
  expect.equal(result2.second.third.fourth.flag, false);
  expect.equal(selectedState, result2.second.third.fourth);
});
