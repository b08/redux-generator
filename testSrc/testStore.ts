import { createStore, IAction, IStore } from "@b08/core-redux";

export type Reducer<T> = (prev: T, action: IAction) => T;

export async function getStore(reducerPath: string): Promise<IStore> {
  const file = await import(reducerPath + "/reducer");
  return createStore(file.reducer);
}

export async function getDispatcher<T = any>(dispatcherPath: string, getter: (f: any) => any,
  store: IStore, id?: string): Promise<T> {
  const file = await import(dispatcherPath + "/dispatcher");
  const constructor = getter(file);
  return new constructor(store, id);
}

export interface StoreAndDispatcher<T> {
  store: IStore;
  dispatcher: T;
}
