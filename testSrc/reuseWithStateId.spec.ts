import { test } from "@b08/test-runner";
import { getDispatcher, getStore } from "./testStore";

test("reuse with stateId, should run reduction", async expect => {
  // arrange
  const store = await getStore("../testData/reuseWithStateId");
  const state = store.getState();
  const rootId = state.stateId;
  const id1 = state.child1.stateId;
  const id2 = state.child2.stateId;
  const dispatcher1 = await getDispatcher("../testData/reuseWithStateId/childFolder",
    f => f.ChildDispatcher, store, id1);
  const dispatcher2 = await getDispatcher("../testData/reuseWithStateId/childFolder",
    f => f.ChildDispatcher, store, id2);
  const dispatcher3 = await getDispatcher("../testData/reuseWithStateId/childFolder",
    f => f.ChildDispatcher, store, "");

  // act
  dispatcher1.flipFlag();
  const result1 = store.getState();
  dispatcher3.flipFlag();
  const result2 = store.getState();
  dispatcher2.flipFlag();
  const result3 = store.getState();
  const selectedState1 = dispatcher1.getState();
  const selectedState2 = dispatcher2.getState();

  // assert
  expect.deepEqual(result1, { stateId: rootId, child1: { stateId: id1, flag: true }, child2: { stateId: id2, flag: false } });
  expect.deepEqual(result2, { stateId: rootId, child1: { stateId: id1, flag: false }, child2: { stateId: id2, flag: true } });
  expect.deepEqual(result3, { stateId: rootId, child1: { stateId: id1, flag: false }, child2: { stateId: id2, flag: false } });
  expect.equal(result3.child1, selectedState1);
  expect.equal(result3.child2, selectedState2);
});
