import { test } from "@b08/test-runner";
import { getDispatcher, getStore } from "./testStore";

test("2 states same file, should run reduction", async expect => {
  // arrange
  const store = await getStore("../testData/twoStatesSameFile");
  const dispatcher1 = await getDispatcher("../testData/twoStatesSameFile", f => f.FirstDispatcher, store);
  const dispatcher2 = await getDispatcher("../testData/twoStatesSameFile", f => f.SecondDispatcher, store);
  const val1 = { val: "123" };
  const val2 = { val: "234" };

  // act
  dispatcher1.setFirstVal(val1);
  dispatcher2.setVal(val2);
  const result = store.getState();
  const selectedState1 =  dispatcher1.getState();
  const selectedState2 =  dispatcher2.getState();

  // assert
  expect.equal(result.val, val1);
  expect.equal(result.second.val, val2);
  expect.equal(result, selectedState1);
  expect.equal(result.second, selectedState2);

});
