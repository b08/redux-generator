import { test } from "@b08/test-runner";
import { getDispatcher, getStore } from "./testStore";

test("depth3 and 2 states, should run reduction", async expect => {
  // arrange
  const store = await getStore("../testData/depth3and2workingStates");
  const dispatcher2 = await getDispatcher("../testData/depth3and2workingStates/subFolder", f => f.SecondDispatcher, store);
  const dispatcher3 = await
    getDispatcher("../testData/depth3and2workingStates/subFolder/subSubFolder", f => f.ThirdDispatcher, store);

  // act
  dispatcher2.flipFlag();
  const result1 = store.getState();
  dispatcher3.flipFlag();
  const result2 = store.getState();
  const selectedState2 = dispatcher2.getState();
  dispatcher2.flipFlag();
  const result3 = store.getState();
  const selectedState3 = dispatcher3.getState();

  // assert
  expect.true(result1.second.flag);
  expect.false(result1.second.third.flag);
  expect.true(result2.second.flag);
  expect.true(result2.second.third.flag);
  expect.false(result3.second.flag);
  expect.true(result3.second.third.flag);
  expect.equal(selectedState2, result2.second);
  expect.equal(selectedState3, result3.second.third);
});
