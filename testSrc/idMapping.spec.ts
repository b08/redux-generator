import { test } from "@b08/test-runner";
import { getDispatcher, getStore } from "./testStore";

test("idMapping, should select correctly", async expect => {
  // arrange
  const store = await getStore("../testData/idMapping");
  const id1 = store.getState().a.e.stateId;
  const id2 = store.getState().f.c.a.stateId;

  const dispatcher1 = await getDispatcher("../testData/idMapping", f => f.EDispatcher, store, id1);
  const dispatcher2 = await getDispatcher("../testData/idMapping", f => f.EDispatcher, store, id2);
  dispatcher1.push(1);
  dispatcher1.push(2);
  dispatcher2.push(3);
  dispatcher2.push(4);

  const root = store.getState();
  const state1 = root.a.e;
  const state2 = root.f.c.a.e;

  async function select(id: string): Promise<any> {
    const dispatcher = await getDispatcher("../testData/idMapping", f => f.EDispatcher, store, id);
    return dispatcher.getState();
  }

  // act
  const select1 = await select(root.stateId);
  const select2 = await select(root.a.stateId);
  const select3 = await select(root.a.e.stateId);
  const select4 = await select(root.f.stateId);
  const select5 = await select(root.f.c.stateId);
  const select6 = await select(root.f.c.d.stateId);
  const select7 = await select(root.f.c.a.stateId);
  const select8 = await select(root.f.c.a.e.stateId);

  // assert
  expect.deepEqual(state1.value, [1, 2]);
  expect.deepEqual(state2.value, [3, 4]);
  expect.equal(select1, state1);
  expect.equal(select2, state1);
  expect.equal(select3, state1);
  expect.equal(select4, state2);
  expect.equal(select5, state2);
  expect.equal(select6, state2);
  expect.equal(select7, state2);
  expect.equal(select8, state2);
});

