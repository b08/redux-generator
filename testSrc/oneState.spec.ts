import { test } from "@b08/test-runner";
import { getDispatcher, getStore } from "./testStore";

test("one state, should initialize", async expect => {
  // arrange
  const store = await getStore("../testData/oneState");

  // act
  const result = store.getState();

  // assert
  expect.deepEqual(result, { stateId: 1, flag: false });
});

test("one state, should run reduction", async expect => {
  // arrange
  const store = await getStore("../testData/oneState");
  const dispatcher = await getDispatcher("../testData/oneState", f => f.SomeDispatcher, store);

  // act
  dispatcher.flip();
  const result = store.getState();
  const selectedState = dispatcher.getState();

  // assert
  expect.deepEqual(result, { stateId: 1, flag: true });
  expect.equal(result, selectedState);
});
