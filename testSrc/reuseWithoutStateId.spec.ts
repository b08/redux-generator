import { test } from "@b08/test-runner";
import { getDispatcher, getStore } from "./testStore";

test("reuse without stateId, should run reduction", async expect => {
  // arrange
  const store = await getStore("../testData/reuseWithoutStateId");
  const dispatcher = await getDispatcher("../testData/reuseWithoutStateId/childFolder", f => f.ChildDispatcher, store);

  // act
  dispatcher.flipFlag();
  const result1 = store.getState();
  dispatcher.flipFlag();
  const result2 = store.getState();

  // assert
  expect.true(result1.child1.flag);
  expect.true(result1.child2.flag);
  expect.false(result2.child1.flag);
  expect.false(result2.child2.flag);
});
