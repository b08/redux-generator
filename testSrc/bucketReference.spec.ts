import { test } from "@b08/test-runner";
import { getDispatcher, getStore } from "./testStore";

test("bucket reference, should run reduction", async expect => {
  // arrange
  const store = await getStore("../testData/bucketReference");
  const dispatcher = await getDispatcher("../testData/bucketReference/subfolder", f => f.SecondDispatcher, store);

  // act
  dispatcher.flipFlag();
  const result1 = store.getState();
  const selectedState = dispatcher.getState();
  dispatcher.flipFlag();
  const result2 = store.getState();

  // assert
  expect.equal(result1.second.flag, true);
  expect.equal(result2.second.flag, false);
  expect.equal(selectedState, result1.second);
});
