export interface ThirdState {
  flag: boolean;
}

export function flipFlag(prev: ThirdState): ThirdState {
  return { ...prev, flag: !prev.flag };
}

