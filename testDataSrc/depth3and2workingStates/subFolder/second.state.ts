import { ThirdState } from "./subSubFolder/third.state";

export interface SecondState {
  third: ThirdState;
  flag: boolean;
}

export function flipFlag(prev: SecondState): SecondState {
  return { ...prev, flag: !prev.flag };
}
