import { SomeState } from "./some.state";

export function flip(prev: SomeState): SomeState {
  return {
    ...prev,
    flag: !prev.flag
  };
}
