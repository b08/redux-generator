export interface FourthState {
  flag: boolean;
}

export function flipFlag(prev: FourthState): FourthState {
  return { ...prev, flag: !prev.flag };
}
