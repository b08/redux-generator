import { ChildState } from "./childFolder/child.state";

export interface RootState {
  child1: ChildState;
  child2: ChildState;
}
