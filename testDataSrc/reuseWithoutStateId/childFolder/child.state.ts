export interface ChildState {
  flag: boolean;
}

export function flipFlag(prev: ChildState): ChildState {
  return { ...prev, flag: !prev.flag };
}
