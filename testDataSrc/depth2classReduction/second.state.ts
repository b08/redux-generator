export interface SecondState {
  flag: boolean;
}

export class Reduction {
  constructor(private prev: SecondState) { }

  public flipFlag(): SecondState {
    return { ...this.prev, flag: !this.prev.flag };
  }
}
