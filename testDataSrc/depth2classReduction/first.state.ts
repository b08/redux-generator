import { SecondState } from "./second.state";

export interface FirstState {
  second: SecondState;
}
