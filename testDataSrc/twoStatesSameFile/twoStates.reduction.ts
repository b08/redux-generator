import { FirstState, SomeType, SecondState } from "./twoStates.state";

export function setFirstVal(prev: FirstState, val: SomeType): FirstState {
  return { ...prev, val };
}

export class SecondReductions {
  constructor(private prev: SecondState) { }

  public setVal(val: SomeType): SecondState {
    return { ...this.prev, val };
  }
}
