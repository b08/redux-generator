export interface FirstState {
  val: SomeType;
  second: SecondState;
}

export interface SecondState {
  val: SomeType;
}

export interface SomeType {
  val: string;
}
