export interface EState {
  value: number[];
  stateId: number;
}

export class Reduction {
  constructor(private prev: EState) { }

  public push(val: number): EState {
    return { ...this.prev, value: [...this.prev.value, val] };
  }
}
