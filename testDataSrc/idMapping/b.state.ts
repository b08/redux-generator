import { AState } from "./a.state";
import { FState } from "./f.state";

export interface BState {
  a: AState;
  f: FState;
  stateId: number;
}
