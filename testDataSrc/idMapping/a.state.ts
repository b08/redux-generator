import { EState } from "./e.state";

export interface AState {
  e: EState;
  stateId: number;
}
