import { DState } from "./d.state";
import { AState } from "./a.state";

export interface CState {
  d: DState;
  a: AState;
  stateId: number;
}
