export interface DState {
  value: string[];
  stateId: number;
}


export class Reduction {
  constructor(private prev: DState) { }

  public setValue(value: string[]): DState {
    return { ...this.prev, value };
  }
}
