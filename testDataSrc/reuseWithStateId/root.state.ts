import { ChildState } from "./childFolder/child.state";

export interface RootState {
  stateId: number;
  child1: ChildState;
  child2: ChildState;
}
