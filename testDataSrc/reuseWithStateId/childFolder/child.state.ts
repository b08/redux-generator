export interface ChildState {
  stateId: number;
  flag: boolean;
}

export function flipFlag(prev: ChildState): ChildState {
  return { ...prev, flag: !prev.flag };
}
